This project calculates the maximum total from a Hells Triangle. This problem, at first (and quickly) look, seems to be a binary tree, but it is a graph one.

Graph depth-first search problems are much complex and it gets bigger exponentially as the input are increased. Nowadays, this is one of the world computer challenges to be solved. You could try some improvements to avoid such brutal force searches, but the worst case will always be exponential.

This implementation calculates the problem using recursion. Recursion can have some problems as high memory consuming, but I think it creates more readable code.


You have some options to execute the project:

- Execute the HellsTriangle.java file using command line, indicating the input file when prompted.
- Execute the HellsTriangleTest.java to run some junit tests (there are some test methods which are looking to the triangle construction and the final total, bigger inputs are not considered).

Optionally you can generate your own random input executing the GenerateInput.java file using command line and indicating the triangle size when prompetd. 