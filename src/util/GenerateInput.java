package util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

import model.Line;
import model.Triangle;
import model.exceptions.BadTriangleException;

/**
 * This class generates a input file containing a random Hells Triangle by the 'size' parameter.
 * 
 * @author Pedro
 *
 */
public class GenerateInput {

	private static Scanner scanner;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Please, inform the size of the Hells Triangle:");
		
		scanner = new Scanner(System.in);
	    int size = scanner.nextInt();
	    
	    Triangle triangle = null;
		try {
			triangle = generateTriangle(size);
			
			//Printing the triangle
			System.out.println(triangle.toString());
			
			PrintWriter writer;
			writer = new PrintWriter(size+"-triangle.txt");
			writer.println(triangle.toString());
			writer.close();
		} catch (BadTriangleException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    
	}

	public static Triangle generateTriangle(int size) throws BadTriangleException {
		//Generating the lines
	    Random randomGenerator = new Random();
	    Line[] lines = new Line[size];
	    for(int i=0; i<size; i++)
	    {
	    	int[] numbers = new int[i+1];
	    	for(int j=0; j<=i; j++)
	    	{
	    		int randomInt = randomGenerator.nextInt(10);
	    		numbers[j] = randomInt;
	    	}
	    	Line line = new Line(numbers, i);
	    	lines[i] = line;
	    }
	    
	    //Creating the triangle
	    Triangle triangle = new Triangle(lines);
		return triangle;
	}

}
