package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import model.Line;
import model.Triangle;
import model.exceptions.BadTriangleException;

public class Util {

	public static String readFile(String filename) {
		//Reading the file
		String triangle = "";
		try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        
	        triangle = sb.toString();
	        
	        
	    } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return triangle;
	}

	public static Triangle createTriangle(String triangle) throws BadTriangleException {
		//Cleaning the string
		triangle = triangle.replace(", [","");
		triangle = triangle.replace("[[","");
		triangle = triangle.replace("]]","");
		triangle = triangle.replace("[]","");
		String[] items = triangle.split("]");
		
		//Creating the lines
		Line[] lines = new Line[items.length];
		for(int i=0; i<items.length; i++)
		{
			int[] numbers = Arrays.asList(items[i].split(","))
					.stream()
					.map(String::trim)
					.mapToInt(Integer::parseInt).toArray();
			lines[i] = new Line(numbers, i);
		}
		
		//Creating the triangle
		Triangle trngle = new Triangle(lines);
		return trngle;
	}
	
	public static Triangle getTriangle(String filename) throws BadTriangleException
	{
		String triangle = readFile(filename);

	    Triangle trngle = createTriangle(triangle);
	    
	    return trngle;
	}
}
