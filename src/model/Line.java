package model;

import java.util.Arrays;

import model.exceptions.BadTriangleException;
import model.exceptions.LineIndexOutOfBoundsException;

/**
 * This class models a triangle line
 * 
 * @author Pedro
 *
 */
public class Line {
	
	private int[] numbers;
	private int index;
	
	public int[] getNumbers() {
		return numbers;
	}
	
	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public int getNumber(int numberIndex) throws LineIndexOutOfBoundsException
	{
		int number = 0;
		if(numberIndex < this.numbers.length)
		{
			number = this.numbers[numberIndex];
		}else
		{
			throw new LineIndexOutOfBoundsException();
		}
		
		return number;
	}
	
	public String toString()
	{
		return Arrays.toString(this.numbers);
	}
	
	public Line(int[] numbers, int index) throws BadTriangleException {
		if(numbers.length == index+1)
		{
			this.numbers = numbers;
			this.index = index;			
		}else
		{
			throw new BadTriangleException();
		}
	}
	
}
