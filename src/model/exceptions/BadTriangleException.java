package model.exceptions;

public class BadTriangleException extends Exception {

	public BadTriangleException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BadTriangleException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BadTriangleException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BadTriangleException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BadTriangleException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
