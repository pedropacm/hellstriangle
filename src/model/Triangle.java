package model;

import java.util.Arrays;

import model.exceptions.LineIndexOutOfBoundsException;

/**
 * This class models the triangle
 * 
 * @author Pedro
 *
 */
public class Triangle {

	private Line[] lines;

	public Line[] getLines() {
		return lines;
	}

	public void setLines(Line[] lines) {
		this.lines = lines;
	}

	public String toString()
	{
		String[] result = new String[this.lines.length];
		for(int i=0; i<this.lines.length; i++)
		{
			result[i] = this.lines[i].toString();
		}
		return Arrays.toString(result);
	}
	
	/**
	 * 
	 * @return
	 */
	public int maxSumRecursive()
	{
		int result = 0;
		try {
			result = this.sumRecursive(0, 0, 0);
			
		} catch (LineIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return result;
	}
	
	private int sumRecursive(int total, int lineIndex, int numberIndex) throws LineIndexOutOfBoundsException
	{
		int result = 0;
		int number = this.lines[lineIndex].getNumber(numberIndex);
		
		if(lineIndex < this.lines.length-1)
		{
			int sumPartial1 = this.sumRecursive(total+number, lineIndex+1, numberIndex);
			int sumPartial2 = this.sumRecursive(total+number, lineIndex+1, numberIndex+1);
			
			result = Math.max(sumPartial1, sumPartial2);
		}else
		{
			result = total+number;
		}
		return result;
	}
	
	public Triangle(Line[] lines) {
		this.lines = lines;
	}
	
	
}
