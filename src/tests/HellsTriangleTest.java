package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import model.Triangle;
import model.exceptions.BadTriangleException;
import model.exceptions.LineIndexOutOfBoundsException;
import util.Util;


public class HellsTriangleTest {
	
	@Test
	public void triangle5SizedSumTest(){
		Triangle t;
		try {
			t = Util.getTriangle("5-triangle.txt");
			
			assertEquals(32, t.maxSumRecursive(), "The 5 sized sample triangle must sum 32.");
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void triangle10SizedSumTest() throws BadTriangleException {
		Triangle t = Util.getTriangle("10-triangle.txt");
		
		assertEquals(64, t.maxSumRecursive(), "The 5 sized sample triangle must sum 64.");
	}
	
	@Test
	public void triangle15SizedSumTest() throws BadTriangleException {
		Triangle t = Util.getTriangle("15-triangle.txt");
		
		assertEquals(102, t.maxSumRecursive(), "The 5 sized sample triangle must sum 102.");
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void badTopTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-badTop.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	@Test
	public void badEmptyTopTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-badEmptyTop.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	@Test
	public void bigLineTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-bigLine.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	

	@Test
	public void littleLineTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-littleLine.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	

	@Test
	public void badBigBottomTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-badBigBottom.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	@Test
	public void badLittleBottomTest()
	{
		exception.expect(BadTriangleException.class);
		Triangle t;
		try {
			t = Util.getTriangle("badTriangle-badLittleBottom.txt");
			t.maxSumRecursive();
		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
}
