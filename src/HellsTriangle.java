import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import model.Line;
import model.Triangle;
import model.exceptions.BadTriangleException;
import util.Util;

/**
 * This Main class calculates the maximum total in a Hells Triangle.
 * 
 * @author Pedro
 *
 */
public class HellsTriangle {

	private static Scanner scanner;
	
	/**
	 * The main method asks for the file name containing a Hells triangle,
	 * calculates the maximum total and shows the result in the console.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Please, inform the Hells Triangle file name:");
		
		scanner = new Scanner(System.in);
	    String filename = scanner.nextLine();
	    
	    String triangle = Util.readFile(filename);

	    Triangle trngle;
		try {
			trngle = Util.createTriangle(triangle);
			
			//Calculating the max sum
			int total = trngle.maxSumRecursive();
			
			System.out.println(total);

		} catch (BadTriangleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
